﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Jerrymandering {
	public class Graph<T> {
		public ISet<Node<T>> nodes;

		public Graph() {
			nodes = new HashSet<Node<T>>();
		}

		public Graph(IComparer<T> comparer) {
			nodes = new SortedSet<Node<T>>(new NodeComparer<T>(comparer));
		}

		public Node<T> getNode(T value) {
			var node = nodes.FirstOrDefault((_node) => {
				return _node.value.Equals(value);
			});
			if (node == null)
				return makeNode(value);
			return node;
		}

		public void addEdgeBetween(T v1, T v2, bool undirected = false) {
			var n1 = getNode(v1);
			var n2 = getNode(v2);
			n1.addNeighbor(n2);

			if (undirected)
				n2.addNeighbor(n1);
		}

		public Node<T> getFirstNode() {
			return nodes.FirstOrDefault((_node) => {
				return _node.active;
			});
		}

		private Node<T> makeNode(T value) {
			var node = new Node<T>(value);
			nodes.Add(node);
			return node;
		}
	}

	public class Node<T> {
		public readonly T value;
		private readonly ISet<Node<T>> neighbors = new HashSet<Node<T>>();
		public bool active = true;

		public Node(T value) {
			this.value = value;
		}

		public void addNeighbor(Node<T> node) {
			this.neighbors.Add(node);
		}

		public IEnumerable<Node<T>> neighborhood {
			get {
				foreach (var neighbor in neighbors) {
					if (neighbor.active) {
						yield return neighbor;
					}
				}
			}
		}
	}

	public class NodeComparer<T> : IComparer<Node<T>>{
		private IComparer<T> comparer;

		public NodeComparer(IComparer<T> comparer){
			this.comparer = comparer;
		}

		public int Compare(Node<T> a, Node<T> b){
			return this.comparer.Compare(a.value, b.value);
		}
	}
}
