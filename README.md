﻿Jerrymandering
==============

This program finds all possible N-township district divisions from a connected graph of townships, which by default is an NxN grid.

To change the size of the grid, change the "SIZE" variable near the top of *Program.cs*.

## Installation

### OS X and Linux

Prerequisite: install [Mono](https://github.com/mono/mono).  There are packages for Mono in Apt-Get, Homebrew, etc.

When Mono is installed, execute this command to compile the program:

	mcs Program.cs Graph.cs

Then execute this command to run the program:

	mono Program.exe

### Windows

Prerequisite: install the .NET framework and C# compiler.  This is usually installed when you install Visual Studio.

Depending on your version of .NET, the C# compiler binary, *csc.exe*, should be located in one of the following locations:

- C:\Windows\Microsoft.NET\Framework\vN.N\csc.exe
- C:\Windows\Microsoft.NET\Framework\vN.N\bin\csc.exe

Replace *vN.N* with your version number, e.g., v3.5 or v4.0.30319.

Now, run this command to compile the program:

	PS> C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe /t:exe /out:Program.exe .\Program.cs .\Graph.cs

Then you can run the program with simply:

	.\Program.exe

## Why C#?

When solving this kind of problem I like to use a strictly typed language.  I don't like to use C++ because the syntax is hard for others to read, and I don't like to use Java because the built-in libraries for things like sets and arrays do not provide "scripting-style" methods like "Union" or "Find".  I chose C# because it supports convenience methods out of the box while still having the readability of Java-style syntax.

## Overall structure of program

We start by making an undirected graph *townships* of the townships.  Each node in the graph represents a township, and each edge represents townships that border each other.  Since all townships must end up in a district, we arbitrarily choose one township to start with; my code always chooses the first township reading left-to-right.  We compute each possible district containing that township based on the "townships" graph; the number of such 5-district townships is on the order of 35.  For each of those potential districts, we recursively apply the same algorithm to the remainder of the graph.

Each district is stored as a 32-bit bitmask of type *uint*.  This means that the highest number of districts the code can support is 32.  One can increase the maximum number of districts to 64 by changing the *uint* to a *ulong*.

## Areas for Improvement

The current algorithm is mostly brute force and does not do much pruning on the recursion.  The following things could be done to improve performance:

1. Store states of the graph in a DP table to reduce the number of repeated sub-problems
2. In the base case of the recursive function, check to make sure that the connected components of the graph are all contain a number of townships that is a multiple of the size of the districts


