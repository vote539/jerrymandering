﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Jerrymandering {
	class MainClass {
		private static int SIZE = 5;

		/*** ENTRYPOINT ***/

		public static void Main(string[] args) {

			// Make the starting graph of townships
			var townships = new Graph<Township>(new TownshipComparer());
			for (byte i = 0; i < SIZE * SIZE; i++) {
				var township = new Township(i);
				if (i >= SIZE)
					townships.addEdgeBetween(township, new Township(i-SIZE), true);
				if (i % SIZE > 0)
					townships.addEdgeBetween(township, new Township(i-1), true);
			}

			// Run the program
			var divisions = divisionsFrom(townships);

			// Print the result
			foreach (var division in divisions) {
				printDivision(townships, division);
				Console.WriteLine("----------");
			}
			Console.WriteLine(divisions.Count);

		}

		/*** LOGIC FUNCTIONS ***/

		// Recursively compute all divisions
		private static ISet<Division> divisionsFrom(Graph<Township> townships){
			var divisions = new HashSet<Division>();

			// Base Case: No townships left to divide.
			// Return an empty division.
			if (townships.getFirstNode() == null) {
				var division = new Division();
				division.districts = new List<uint>();
				divisions.Add(division);
				return divisions;
			}

			// Recursive Step
			var districts = districtsFrom(townships);
			foreach (var district in districts) {

				// Perform recursion
				applyMask(townships, district, false);
				var _divisions = divisionsFrom(townships);
				applyMask(townships, district, true);

				// Add ourselves to the division
				foreach (var division in _divisions) {
					division.districts.Add(district);
				}

				// Merge into the main division set
				divisions.UnionWith(_divisions);
			}

			return divisions;
		}

		// Given a township map, return all districts containing the upper-left township
		private static ISet<uint> districtsFrom(Graph<Township> townships) {
			var partials = new HashSet<uint>();

			// Make the first partial district containing the upper-left township
			partials.Add(mask(townships.getFirstNode().value));

			// Iteratively add townships until we have SIZE townships in all districts
			for (int layernum = 1; layernum < SIZE; layernum++) {
				var oldPartials = partials;
				partials = new HashSet<uint>();

				foreach (var partial in oldPartials) {
					partials.UnionWith(partialsFrom(townships, partial));
				}
			}

			return partials;
		}

		// Given a township map and a partially-complete district, return all partial districts
		// containing one township more than the input partial district.
		private static ISet<uint> partialsFrom(Graph<Township> townships, uint partial) {
			var newPartials = new HashSet<uint>();

			// For each township...
			foreach (var township in townships.nodes) {

				// If the township is in our current partial district...
				if ((partial & mask(township.value)) > 0) {

					// For each of the neighboring townships...
					foreach (var neighbors in township.neighborhood) {

						// If the neighbor is not in our current partial district...
						if ((partial & mask(neighbors.value)) == 0) {

							// Then make a new partial district that includes the neighbor.
							newPartials.Add(partial | mask(neighbors.value));
						}
					}
				}
			}

			return newPartials;
		}

		private static uint mask(Township v) {
			return (uint)1 << v.val;
		}

		/*** UTILITY FUNCTIONS **/

		// Enable or disable all townships in the graph corresponding the the given district
		private static void applyMask(Graph<Township> townships, uint district, bool active){
			foreach (var township in townships.nodes) {
				if ((district & mask(township.value)) > 0) {
					township.active = active;
				}
			}
		}

		private static void printPartial(Graph<Township> townships, string letter, uint partial) {
			var index = 0;
			foreach (var township in townships.nodes) {
				Console.Write((partial & mask(township.value)) == 0 ? "." : letter);
				if (index % SIZE == SIZE - 1)
					Console.WriteLine();
				index++;
			}
		}

		private static void printDivision(Graph<Township> townships, Division division){
			var i = 0;
			foreach (var township in townships.nodes) {
				var j = 0;
				foreach (var district in division.districts) {
					if ((district & mask(township.value)) > 0) {
						Console.Write((char)(64 + SIZE - j));
					}
					j++;
				}

				if (i % SIZE == SIZE - 1)
					Console.WriteLine();
				i++;
			}
		}

		private struct Division {
			public IList<uint> districts;
		}

		private struct Township {
			public readonly byte val;

			public Township(byte val){
				this.val = val;
			}

			public Township(int val){
				this.val = (byte) val;
			}
		}

		private class TownshipComparer : IComparer<Township> {
			public int Compare(Township a, Township b) {
				if (a.val < b.val)
					return -1;
				if (a.val > b.val)
					return 1;
				return 0;
			}
		}
	}
}
